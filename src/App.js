import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import "./App.css";

const App = () => {
  const [page, setPage] = useState(-1);
  const [element, setElement] = useState(null);
  const [image, setImage] = useState([]);
  const [loading, setLoading] = useState(false);

  const updateImages = async (page) => {
    setLoading(true);
    const { data } = await axios.get(
      `https://picsum.photos/v2/list?page=${page}&limit=10`
    );
    const imageIds = data.map((el) => el.id);
    setImage([...image, ...imageIds]);
    setLoading(false);
  };

  const observer = useRef(
    new IntersectionObserver(
      async (entries) => {
        const first = entries[0];
        console.log(first.isIntersecting);
        if (first.isIntersecting) {
          setPage(page => page + 1);
        }
      },
      { threshold: 1 }
    )
  );

  useEffect(() => {
    const currentElement = element;
    const currentObserver = observer.current;
    if (currentElement) {
      currentObserver.observe(currentElement);
    }

    return () => {
      if (currentElement) {
        currentObserver.unobserve(currentElement);
      }
    };
  }, [element]);

  useEffect(() => {
    updateImages(page);
  }, [page]);

  return (
    <div>
      {image.map((el, i) => (
        <div className="grid-container" key={i}>
          <img
            className="grid-item"
            key={i}
            src={`https://picsum.photos/id/${el}/400/400`}
          />
        </div>
      ))}
      <div ref={setElement}>{loading ? "Loading..." : null}</div>
    </div>
  );
};

export default App;
